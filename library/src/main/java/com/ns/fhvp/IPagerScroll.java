package com.ns.fhvp;

/**
 * Created by xiaolf1 on 2015/1/21 0021.
 */
public interface IPagerScroll {

    public boolean isFirstChildOnTop();

}
