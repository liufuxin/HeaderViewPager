package com.ns.fhvp.sample;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.ns.fhvp.IPagerScroll;
import com.ns.fhvp.PagerScrollUtils;

/**
 * Created by xiaolf1 on 2015/1/19 0019.
 */
public class ListFragment extends Fragment implements IPagerScroll {

    private ListView mListView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my, container, false);
        mListView = (ListView) view.findViewById(R.id.listView);
        String[] data = new String[]{"item1", "item2", "item3", "item4", "item5", "item6", "item7", "item8", "item9", "item10", "item11", "item12", "item13", "item14", "item15", "item16", "item17", "item18"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, data);
        mListView.setAdapter(adapter);
        return view;
    }

    @Override
    public boolean isFirstChildOnTop() {
        return PagerScrollUtils.isFirstChildOnTop(mListView);
    }

}
