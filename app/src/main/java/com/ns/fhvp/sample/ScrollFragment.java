package com.ns.fhvp.sample;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.ns.fhvp.IPagerScroll;
import com.ns.fhvp.PagerScrollUtils;

/**
 * Created by xiaolf1 on 2015/1/21 0021.
 */
public class ScrollFragment extends Fragment implements IPagerScroll {

    private ScrollView scrollView = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        scrollView = (ScrollView) inflater.inflate(R.layout.fragment_scroll, container, false);
        return scrollView;
    }

    @Override
    public boolean isFirstChildOnTop() {
        return PagerScrollUtils.isFirstChildOnTop(scrollView);
    }

}
