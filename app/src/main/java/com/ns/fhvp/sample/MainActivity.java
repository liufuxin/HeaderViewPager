package com.ns.fhvp.sample;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;

import com.ns.fhvp.IPagerScroll;
import com.ns.fhvp.TouchPanelLayout;


public class MainActivity extends Activity implements TouchPanelLayout.IConfigCurrentPagerScroll, TouchPanelLayout.OnViewUpdateListener {

    private ViewPager myViewPager = null;
    private TouchPanelLayout mTouchPanelLayout;
    private FragmentAdapter mAdapter;
    private Drawable mAbBg = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAbBg = new ColorDrawable(getResources().getColor(R.color.blue_light));
        mAbBg.setAlpha(0);
        getActionBar().setBackgroundDrawable(mAbBg);

        mTouchPanelLayout = (TouchPanelLayout) LayoutInflater.from(this).inflate(R.layout.activity_main, null, false);
        setContentView(mTouchPanelLayout);
        mTouchPanelLayout.setConfigCurrentPagerScroll(this);
        mTouchPanelLayout.setOnViewUpdateListener(this);

        myViewPager = (ViewPager) findViewById(R.id.viewPager);
        myViewPager.setOffscreenPageLimit(1);
        mAdapter = new FragmentAdapter(getFragmentManager(), this);
        mAdapter.addTab(ListFragment.class, "MyFragment1", null);
        mAdapter.addTab(ScrollFragment.class, "ScrollFragment", null);
        mAdapter.addTab(ListFragment.class, "ScrollFragment2", null);
        myViewPager.setAdapter(mAdapter);

    }


    @Override
    public IPagerScroll getCurrentPagerScroll() {
        Fragment fragment = mAdapter.getCurrentFragment(myViewPager.getCurrentItem());
        if (fragment != null && fragment instanceof IPagerScroll) {
            return (IPagerScroll) fragment;
        }
        return null;
    }

    @Override
    public float getActionBarHeight() {
        return getActionBar().getHeight();
    }

    @Override
    public void onAlphaChanged(int alpha) {
        if (mAbBg != null) {
            mAbBg.setAlpha(alpha);
        }
    }
}
